library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity top is
  Port (
    sysclk  : in  std_logic;
    btn     : in  std_logic_vector(1 downto 0);
    led     : out std_logic_vector(1 downto 0);
    led0_b, led0_g, led0_r : out std_logic
  );
end top;

architecture Behavioral of top is

  signal counter : unsigned(22 downto 0);
  signal color : unsigned(2 downto 0);
  signal debounce : unsigned(19 downto 0);
  signal btn_pipe : std_logic_vector(1 downto 0);

begin
  
  counter <= counter + 1 when rising_edge(sysclk);
  led(0) <= counter(counter'high)   when btn(1) = '0' else '0';
  led(1) <= counter(counter'high-1) when btn(1) = '0' else '0';

  process(sysclk)
  begin
    if rising_edge(sysclk) then
      if btn_pipe = "10" then
        color <= color + 1;
      end if;

      btn_pipe <= btn_pipe(0) & debounce(debounce'high);

      if    btn(0) = '1' and debounce /= (debounce'range => '1') then debounce <= debounce + 1;
      elsif btn(0) = '0' and debounce /= (debounce'range => '0') then debounce <= debounce - 1;
      end if;
    end if;
  end process;

  process(color, counter)
  begin
    if counter(1 downto 0) = "11" then
      (led0_b, led0_g, led0_r) <= std_logic_vector(color);
    else
      led0_b <= '1';
      led0_g <= '1';
      led0_r <= '1';
    end if;
  end process;

end Behavioral;
